from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin

from tags.models import Tag


# Create your views here.
def show_tags(request):
    context = {
        "tags": Tag.objects.all() if Tag else None,
    }
    return render(request, "tags/list.html", context)
